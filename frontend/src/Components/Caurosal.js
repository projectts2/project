import React from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import img1 from './images/img1.jpeg'
import img2 from './images/img2.jpeg'
import img3 from './images/img3.jpeg'

function Caurosal() {
  const carouselStyles = {
    height:'80vh',
    margin: " 20px 0px",
  };
 
 
  return (
    <div>
      <div>
        <Carousel
          autoPlay
          interval="5000"
          transitionTime="5000"
          showThumbs={false}
          infiniteLoop={true}
        >
          <div style={carouselStyles}>
            <img
              src={img1}
              alt=""
            />
          </div>
          <div style={carouselStyles }>
            <img
              src={img2}
              alt=""
            />
          </div>
          <div style={carouselStyles}>
            <img
              src={img3}
              alt=""
            />
          </div>
        </Carousel>
      </div>
    </div>
  );
}

export default Caurosal