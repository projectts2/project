
import './App.css';
import NavigationBar from './Components/NavigationBar';
import Home from './pages/Home';
import ContactUs from './pages/ContactUs';
import Cars from './pages/Cars';
import Login from './pages/Login';
import Register from './pages/Register';
import Footer from './pages/Footer';


import {BrowserRouter,Routes,Route} from 'react-router-dom';
import Caurosal from './Components/Caurosal';




function App() {
  return (
    <div>
      
      <BrowserRouter>
      <NavigationBar />
      {/* <Caurosal /> */}
      
      
     <Routes>
      <Route path='/' element={<Home />} />
      <Route path='contact' element={<ContactUs />} />
      <Route path='cars' element={<Cars />} />
      <Route path='login' element={<Login />} />
      <Route path='register' element={<Register />} />
      
    

      </Routes>
      </BrowserRouter>
      <Footer />

    </div>
  );
}

export default App;