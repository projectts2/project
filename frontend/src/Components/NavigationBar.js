import React from 'react'
import {Link} from 'react-router-dom'
import logo from './images/logo.webp'

function NavigationBar() {
  return (
    <div>
       <nav className="navbar navbar-expand-lg navbar-light bg-light">
         <img src={logo} style={{height:'4rem', width:'6rem'}} alt=''></img> 
  <h1 style={{fontsize:'1rem',color:'orange',fontSize:'2rem'}}>Travel India</h1>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavDropdown">
    <ul className="navbar-nav ml-auto">
      <li className="nav-item active">
        <Link to="/" className='text-white nav-link'><b>Home</b></Link>
      </li>
     
      <li className="nav-item">
      <Link to="/contact" className='text-white nav-link'><b>ContactUs</b></Link>
      </li>

      <li className="nav-item">
      <Link to="/aboutus" className='text-white nav-link'><b>AboutUs</b></Link>
      </li>
      <li className="nav-item">
      <Link to="/login" className='text-white nav-link'><b>Login</b></Link>
      </li>
      <li className="nav-item">
      <Link to="/register" className='text-white nav-link'><b>Register</b></Link>
      </li>       
     </ul>
  </div>
</nav> 
</div>
  )
}

export default NavigationBar