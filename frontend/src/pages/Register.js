import React, { useState } from 'react'
import axios from 'axios'
import {Input,FormGroup,Label,Form,Col,Row,Button} from 'reactstrap'

function Register() {
  let registerForm={border:'1px solid gray', 
  width:'500px',
  padding:'1rem',
  margin:'3rem auto',
  borderRadius:'20px',
  backgroundColor:'pink'

  }
  if(window.innerWidth>600){
    registerForm.width='40%'
    registerForm.margin='2rem auto'
  }
  if(window.innerWidth<=600){
    registerForm.width='40%'
    registerForm.margin='2rem auto'
  }

 
  let [formdata,setFormData] = useState({
    email:'',
    password:'',
    address:'',
    mobilenumber:'',
    check:'false'


  })
  const handleOnchange = (e) =>{
   let {name,value,type,checked} = e.target  
   let inputValue = type === 'checkbox' ? checked:value
     
    setFormData({
     ...formdata,
       [name]:inputValue

     
     
    })
  
  }
  const handlesubmit = (e)=>{
    e.preventDefault();
    // console.log(formdata)
  let register=async()=> {
  try{
    let response=await axios.post("http://localhost:3001/fetch2",formdata)
    console.log(response)
  }
  catch(err) {
    console.log(err)
  
  }
  
}
register()
  }

  return (
    <div style={registerForm}>
      <Form>
      <FormGroup>
    <Label for="name">
      User Name
    </Label>
    <Input
      id="name"
      name="UserName"
      placeholder="name"
    />
  </FormGroup>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="Email"
          type="email"
          value= {formdata.email}
          onChange={handleOnchange}
          
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password"
          type="password"
          value = {formdata.password}
          onChange={handleOnchange}

        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="Address"
      value= {formdata.address}
      onChange={handleOnchange}

    />
  </FormGroup>
  <FormGroup>
    <Label for="mobile">
      MobileNumber
    </Label>
    <Input
      id="mobile"
      name="mobilenumber"
      placeholder="Number"
      value= {formdata.mobilenumber}
      onChange={handleOnchange}

    />
  </FormGroup>

  
  
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup>
  <Button   onClick={handlesubmit} type='submit'>
   Register
  </Button>
</Form>
    </div>
  )
}

export default Register